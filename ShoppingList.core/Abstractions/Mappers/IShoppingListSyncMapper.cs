﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Core.Abstractions.Mappers
{
    public interface IShoppingListSyncMapper: ISyncMapper<ShoppingList.Infra.Models.Repo.ShoppingList, ShoppingList.Shared.Models.ShoppingList>
    {

    }
}
