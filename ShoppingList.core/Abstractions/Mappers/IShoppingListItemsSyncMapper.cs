﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Core.Abstractions.Mappers
{
    public interface IShoppingListItemsSyncMapper: ISyncMapper<ShoppingList.Infra.Models.Repo.ShoppingListItem, ShoppingList.Shared.Models.ShoppingListItem>
    {
    }
}
