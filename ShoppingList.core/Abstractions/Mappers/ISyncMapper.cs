﻿using System.Threading.Tasks;

namespace ShoppingList.Core.Abstractions.Mappers
{
    public interface ISyncMapper<TFrom, TTo>
    {
        TFrom Map(TTo converted);

        TTo Map(TFrom original);
    }
}
