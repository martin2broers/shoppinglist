﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Core.Abstractions.Mappers
{
    public interface IDishesSyncMapper: ISyncMapper<ShoppingList.Infra.Models.Repo.Dish, ShoppingList.Shared.Models.Dish>
    {
    }
}
