﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ShoppingList.Core.Models;

namespace ShoppingList.Core.Abstractions
{
    public interface IBasicQuery<T>
    {
        Task<CoreResult<T>> GetById(int id, CancellationToken cancellationToken);

        Task<CoreResults<T>> GetByIds(IEnumerable<int> ids, CancellationToken cancellationToken);

        Task<CoreResults<T>> All(int skip, int limit, CancellationToken cancellationToken);

        Task<CoreResult<T>> Create(T entity, CancellationToken cancellationToken);

        Task<CoreResult<T>> Set(T entity, CancellationToken cancellationToken);

        Task<CoreResult<T>> Remove(int id, CancellationToken cancellationToken);
    }
}
