﻿using System.Threading;
using System.Threading.Tasks;
using ShoppingList.Core.Models;

namespace ShoppingList.Core.Abstractions
{
    public interface IShoppingListItemsService: IBasicQuery<ShoppingList.Shared.Models.ShoppingListItem>
    {
        public Task<CoreResults<ShoppingList.Shared.Models.ShoppingListItem>> AllOrphaned(int skip, int limit, CancellationToken token);
    }
}
