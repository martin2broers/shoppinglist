﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingList.Shared.Models;

namespace ShoppingList.Core.Abstractions
{
    public interface IDishesService : IBasicQuery<ShoppingList.Shared.Models.Dish>
    {

    }
}
