﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ShoppingList.Core.Abstractions;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Core.Models;
using ShoppingList.Infra.Abstractions;

namespace ShoppingList.Core.Services
{
    public class BasicQuery<TFrom, TTo> : IBasicQuery<TTo>
    {
        private readonly IBasicSql<TFrom> _dbConnection;
        private readonly ISyncMapper<TFrom, TTo> _syncMapper;

        protected BasicQuery(IBasicSql<TFrom> dbConnection, ISyncMapper<TFrom, TTo> syncMapper)
        {
            _dbConnection = dbConnection;
            _syncMapper = syncMapper;
        }

        public async Task<CoreResult<TTo>> GetById(int id, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.GetById(id, cancellationToken);
            return new CoreResult<TTo>
            {
                Result = result.Result != null ? _syncMapper.Map(result.Result) : default,
                Status = result.Status
            };
        }

        public async Task<CoreResults<TTo>> GetByIds(IEnumerable<int> ids, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.GetByIds(ids, cancellationToken);
            var converted = result.Results.Select(e => _syncMapper.Map(e));

            return new CoreResults<TTo>
            {
                Result = converted,
                Status = result.Status
            };
        }

        public async Task<CoreResults<TTo>> All(int skip, int limit, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.All(skip, limit,null, cancellationToken);
            var converted = result.Results.Select(e => _syncMapper.Map(e));

            return new CoreResults<TTo>
            {
                Result = converted,
                Status = result.Status
            };
        }

        public async Task<CoreResult<TTo>> Create(TTo entity, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.Insert(_syncMapper.Map(entity), cancellationToken);

            return new CoreResult<TTo>
            {
                Result = result.Result != null ? _syncMapper.Map(result.Result) : default,
                Status = result.Status
            };
        }

        public async Task<CoreResult<TTo>> Set(TTo entity, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.Update(_syncMapper.Map(entity), cancellationToken);

            return new CoreResult<TTo>
            {
                Result = result.Result != null ? _syncMapper.Map(result.Result) : default,
                Status = result.Status
            };
        }

        public async Task<CoreResult<TTo>> Remove(int id, CancellationToken cancellationToken)
        {
            var result = await _dbConnection.Delete(id, cancellationToken);

            return new CoreResult<TTo>
            {
                Result = default,
                Status = result.Status
            };
        }

    }
}