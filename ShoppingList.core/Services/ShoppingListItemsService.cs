﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ShoppingList.Core.Abstractions;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Core.Models;
using ShoppingList.Infra.Abstractions;
using ShoppingList.Infra.Services;
using ShoppingList.Shared.Models;

namespace ShoppingList.Core.Services
{
    public class ShoppingListItemsService: BasicQuery<ShoppingList.Infra.Models.Repo.ShoppingListItem, ShoppingList.Shared.Models.ShoppingListItem>, IShoppingListItemsService
    {
        private readonly IShoppingListItemRepo _repo;
        private readonly IShoppingListItemsSyncMapper _syncMapper;

        public ShoppingListItemsService(IShoppingListItemRepo repo, IShoppingListItemsSyncMapper mapper): base(repo, mapper)
        {
            _repo = repo;
            _syncMapper = mapper;
        }

        public async Task<CoreResults<ShoppingListItem>> AllOrphaned(int skip, int limit, CancellationToken token)
        {
            var dbResult = await _repo.All(skip, limit, item => item.Dish == null, token);
            var converted = dbResult.Results.Select(e => _syncMapper.Map(e));

            return new CoreResults<ShoppingListItem>
            {
                Result = converted,
                Status = dbResult.Status
            };
        }
    }
}
