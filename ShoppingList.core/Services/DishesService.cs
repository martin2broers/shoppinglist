﻿using ShoppingList.Core.Abstractions;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Infra.Abstractions;

namespace ShoppingList.Core.Services
{
    public class DishesService: BasicQuery<ShoppingList.Infra.Models.Repo.Dish, ShoppingList.Shared.Models.Dish>, IDishesService
    {
        public DishesService(IDishesRepo dishes, IDishesSyncMapper mapper) : base(dishes, mapper)
        {

        }
    }
}
