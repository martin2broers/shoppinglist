﻿using System.Collections.Generic;
using ShoppingList.Shared.Models;

namespace ShoppingList.Core.Models
{
    public class CoreResults<T>
    {
        public ResultStatus Status { get; set; }

        public IEnumerable<T> Result { get; set; }
    }
}
