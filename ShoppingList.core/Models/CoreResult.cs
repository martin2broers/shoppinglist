﻿using ShoppingList.Shared.Models;

namespace ShoppingList.Core.Models
{
    public class CoreResult<T>
    {
        public ResultStatus Status { get; set; }

        public T Result { get; set; }
    }
}
