﻿using System;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Infra.Models.Repo;
using ShoppingList.Shared;

namespace ShoppingList.Core.Mapper
{
    public class ShoppingListItemSyncMapper: IShoppingListItemsSyncMapper
    {
        private readonly ResolveAble<IDishesSyncMapper> _dishesSyncMapper;
        private readonly ResolveAble<IShoppingListSyncMapper> _shoppingListSyncMapper;

        public ShoppingListItemSyncMapper(ResolveAble<IDishesSyncMapper> dishesSyncMapper, ResolveAble<IShoppingListSyncMapper> shoppingListSyncMapper)
        {
            _dishesSyncMapper = dishesSyncMapper;
            _shoppingListSyncMapper = shoppingListSyncMapper;
        }


        public ShoppingListItem Map(Shared.Models.ShoppingListItem converted)
        {
            return new ShoppingListItem
            {
                Id = converted.Id.GetValueOrDefault(),
                ItemName = converted.ItemName,
                Amount = converted.Amount,
                AmountType = converted.AmountType,
                DishId = converted.Dish != null ? (int?) converted.Dish.Id : null,
                Dish = converted.Dish != null ? _dishesSyncMapper.Value.Map(converted.Dish) : null,
                ShoppingList = converted.ShoppingList != null ? _shoppingListSyncMapper.Value.Map(converted.ShoppingList) : null
            };
        }

        public Shared.Models.ShoppingListItem Map(ShoppingListItem original)
        {
            return new Shared.Models.ShoppingListItem
            {
                Id = original.Id,
                ItemName = original.ItemName,
                Amount = original.Amount,
                AmountType = original.AmountType,
                Dish = original.Dish != null ? _dishesSyncMapper.Value.Map(original.Dish) : null,
                ShoppingList = original.ShoppingList != null ? _shoppingListSyncMapper.Value.Map(original.ShoppingList) : null
            };
        }
    }
}
