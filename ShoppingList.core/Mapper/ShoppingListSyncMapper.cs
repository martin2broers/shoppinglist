﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingList.Core.Abstractions.Mappers;

namespace ShoppingList.Core.Mapper
{
    public class ShoppingListSyncMapper: IShoppingListSyncMapper
    {
        public Infra.Models.Repo.ShoppingList Map(Shared.Models.ShoppingList converted)
        {
            return new Infra.Models.Repo.ShoppingList
            {
              Id = converted.Id,
              Name = converted.Name
            };
        }

        public Shared.Models.ShoppingList Map(Infra.Models.Repo.ShoppingList original)
        {
            return new Shared.Models.ShoppingList
            {
                 Id = original.Id,
                 Name = original.Name
            };
        }
    }
}
