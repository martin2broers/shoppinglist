﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Shared;
using ShoppingList.Shared.Models;

namespace ShoppingList.Core.Mapper
{
    public class DishesSyncMapper: IDishesSyncMapper
    {
        private readonly ResolveAble<IShoppingListItemsSyncMapper> _iIngredientsSyncMapper;

        public DishesSyncMapper(ResolveAble<IShoppingListItemsSyncMapper> iIngredientsSyncMapper)
        {
            _iIngredientsSyncMapper = iIngredientsSyncMapper;
        }

        public Dish Map(Infra.Models.Repo.Dish original)
        {
            return new Dish
            {
                Id = original.Id,
                Name = original.Name,
                FeaturedImage = original.FeaturedImage,
                HowTo = original.HowTo,
                Ingredients = original.Ingredients != null ? original.Ingredients.Select(e => _iIngredientsSyncMapper.Value.Map(e)) : null
            };
        }

        public Infra.Models.Repo.Dish Map(Dish converted)
        {
            return new Infra.Models.Repo.Dish
            {
                Id = converted.Id,
                Name = converted.Name
            };
        }
    }
}
