#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster
WORKDIR /app/publish/
EXPOSE 5000
EXPOSE 5001

RUN apt-get update -y
RUN apt install nodejs npm -y

#COPY . .
COPY [".", "/src"]
RUN dotnet restore "/src/ShoppingList/ShoppingList.Web.csproj"

RUN cd "/src/ShoppingList/" && npm install
RUN cd "/src/ShoppingList/" && npm run build
RUN dotnet build "/src/ShoppingList/ShoppingList.Web.csproj" -c Release -o /app/build
RUN dotnet publish "/src/ShoppingList/ShoppingList.Web.csproj" -c Release -o /app/publish

#delete source
RUN rm -r "/src/"

RUN apt-get pruge nodejs -y
RUN apt-get pruge npm -y

ENTRYPOINT ["dotnet", "ShoppingList.Web.dll"]