﻿const path = require('path');

//@ts-ignore
const debug = process.env.NODE_ENV !== 'production';

function genExecution(name, i, o) {
    return {
        devtool: debug ? 'inline-sourcemap' : false,
        mode: debug ? 'development' : 'production',
        optimization: {
            // We no not want to minimize our code.
            minimize: !debug
        },
        entry: {
            [name]: i,
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                }
            ]
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            filename: debug ? name + '.js' : name + '.js',
            path: path.resolve(__dirname, o)
        }
    };
}



module.exports = [
    genExecution('index', './Scripts/ShoppingListItems/index.ts', 'wwwroot/lib/app/shoppinglistitems'),
    genExecution('index', './Scripts/Dishes/index.ts', 'wwwroot/lib/app/dishes'),
    genExecution('create', './Scripts/Dishes/Create.ts', 'wwwroot/lib/app/dishes'),
];