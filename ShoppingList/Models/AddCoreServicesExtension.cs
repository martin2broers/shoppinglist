﻿using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Core.Abstractions;
using ShoppingList.Core.Services;

namespace ShoppingList.Web.Models
{
    public static class AddCoreServicesExtension
    {
        public static void AddCoreServices(this IServiceCollection services)
        {
            services.AddScoped<IDishesService, DishesService>();
            services.AddScoped<IShoppingListItemsService, ShoppingListItemsService>();
        }
    }
}
