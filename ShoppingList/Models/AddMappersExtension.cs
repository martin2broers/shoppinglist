﻿using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Core.Abstractions.Mappers;
using ShoppingList.Core.Mapper;
using ShoppingList.Shared;

namespace ShoppingList.Web.Models
{
    public static class AddMappersExtension
    {
        public static void AddMappers(this IServiceCollection services)
        {

            //singletons because why not performance
            services.AddSingleton(new ResolveAble<IDishesSyncMapper>());
            services.AddSingleton(new ResolveAble<IShoppingListSyncMapper>());
            services.AddSingleton(new ResolveAble<IShoppingListItemsSyncMapper>());


            services.AddSingleton<IShoppingListSyncMapper>(s =>
            {
                 var toReturn = new ShoppingListSyncMapper();
                 s.GetService<ResolveAble<IShoppingListSyncMapper>>().Value = toReturn;
                 return toReturn;
            });

            services.AddSingleton<IDishesSyncMapper>(s =>
            {
                var toReturn = new DishesSyncMapper(s.GetService<ResolveAble<IShoppingListItemsSyncMapper>>());
                s.GetService<ResolveAble<IDishesSyncMapper>>().Value = toReturn;
                return toReturn;
            });

            services.AddSingleton<IShoppingListItemsSyncMapper>(s =>
            {
                var toReturn = new ShoppingListItemSyncMapper(
                    s.GetService<ResolveAble<IDishesSyncMapper>>(),
                    s.GetService<ResolveAble<IShoppingListSyncMapper>>()
                );
                s.GetService<ResolveAble<IShoppingListItemsSyncMapper>>().Value = toReturn;
                return toReturn;
            });
        }
    }
}