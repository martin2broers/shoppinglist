﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ShoppingList.Web.Models.Views
{
    public class DishCreation
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Display(Name = "File")]
        public IFormFile FeaturedImage { get; set; }

        public string HowTo { get; set; }

        public IEnumerable<int> Ingredients { get; set; }
    }
}
