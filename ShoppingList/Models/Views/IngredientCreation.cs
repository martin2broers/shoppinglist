﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using ShoppingList.Shared.Models;

namespace ShoppingList.Web.Models.Views
{
    public class IngredientCreation
    {
        [JsonPropertyName("id")]
        public int? Id { get; set; }

        [JsonPropertyName("name")]
        public string ItemName { get; set; }

        [JsonPropertyName("amount")]
        public double Amount { get; set; }

        [JsonPropertyName("amountType")]
        public AmountType AmountType { get; set; }
    }
}