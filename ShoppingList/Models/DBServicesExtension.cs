﻿using Microsoft.Extensions.DependencyInjection;
using ShoppingList.Infra.Abstractions;
using ShoppingList.Infra.Repositories;

namespace ShoppingList.Web.Models
{
    public static class DbServicesExtension
    {
        public static void AddShoppingListInfra(this IServiceCollection services)
        {
            services.AddScoped<IDishesRepo, DishesRepo>();
            services.AddScoped<IShoppingListItemRepo, ShoppingListItemRepo>();
        }
    }
}
