﻿import { WizardStep} from "./Wizard/WizardStep";

export class ButtonProperty {
    text: string;
    disabled: boolean;
    onClick: (step: WizardStep) => void;

    constructor(text: string, disabled: boolean, onClick: (step: WizardStep) => void) {
        this.text = text;
        this.disabled = disabled;
        this.onClick = onClick;
    }
}