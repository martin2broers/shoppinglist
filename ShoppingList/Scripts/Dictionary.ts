﻿import {Optional, Some, None} from "./Optional"

export interface IDictionary<TKey, TValue> {

    get(key: TKey): Optional<TValue>;

    set(key: TKey, o: TValue): boolean;
}

export class Dictionary<TKey, TValue> implements IDictionary<TKey, TValue> {
    private innerObject: any = {}

    get(key: TKey): Optional<TValue> {
        if (this.innerObject.hasOwnProperty(((key) as any) as string)) {
            return new Some(this.innerObject[((key) as any) as string]);
        }
        return new None();
    }

    set(key: TKey, o: TValue): boolean {
        if (this.innerObject.hasOwnProperty(((key) as any) as string)) {
            return false;
        }
        this.innerObject[((key) as any) as string] = o;
        return true;
    }
}