﻿export class Optional<T> {
    protected inner: T;

    protected constructor(value: T) {
        this.inner = value;
    }

    isDefined(): boolean {
        if (this.inner) {
            return true;
        }
        return false;
    }

    get(): T {
        return this.inner;
    }
}


export class Some<T> extends Optional<T> {
    constructor(value: T) {
        super(value);
    }
}

export class None extends Optional<any> {
    constructor() {
        super(null);
    }
}