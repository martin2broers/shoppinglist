﻿import { Optional, Some, None } from "./Optional"

export interface IUniqueList<T> {
    get(index: number): Optional<T>;

    add(o: T): boolean; 
}

export class UniqueList<T> implements IUniqueList<T> {
    private inner: T[] = [];

    get(index: number): Optional<T> {
        if (this.inner[index]) {
            return new Some(this.inner[index]);
        }
        return new None();
    }

    add(o: T): boolean {
        if (this.inner.indexOf(o) > 0) {
            return false;
        }
        this.inner.push(o);
        return true;
    }
}