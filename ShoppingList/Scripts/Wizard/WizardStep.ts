﻿import { Modal } from "../Modal";
import { ButtonProperty } from "../ButtonProperty";

export class AjaxRequest {
    private readonly _body: any;
    private readonly _url: string;

    constructor(url: string, body: any) {
        this._body = body;
        this._url = url;
    }

    get body(): any { return this._body; }

    get url(): string { return this._url; }

}

export class WizardStep {

    private readonly title: string;
    private readonly ajaxUrl: (previousStep: WizardStep | null) => AjaxRequest;
    private readonly modal: Modal;
    private readonly firstButtonCreation: (() => ButtonProperty);
    private readonly secondButtonCreation: (() => ButtonProperty);
    private readonly afterBody: (step: WizardStep, modal: Modal) => void;
    private completeWizardStep: ((result: any) => void) | null = null;
    private wizardStepResult: any = null;

    constructor(
        modal: Modal,
        title: string,
        ajaxRequest: (previousStep: WizardStep | null) => AjaxRequest,
        firstButton: () => ButtonProperty,
        secondButton: () => ButtonProperty,
        afterBody: (step: WizardStep, modal: Modal) => void
    ) {
        this.afterBody = afterBody;

        this.modal = modal;
        this.title = title;
        this.ajaxUrl = ajaxRequest;
        this.firstButtonCreation = firstButton;
        this.secondButtonCreation = secondButton;
    }

    getResult(): any { return this.wizardStepResult; }

    registerCompletion(callback: (result: any) => void) {
        this.completeWizardStep = callback;
    }

    complete(result: any) {
        this.wizardStepResult = result;
        if (this.completeWizardStep !== null) {
            this.completeWizardStep(result);
        }
    }

    fillBody(previousStep: WizardStep | null) {
        const ajaxAction = this.ajaxUrl(previousStep);
        if (ajaxAction.body && ajaxAction.body !== null) {
            const requestHeaders = new Headers();
            requestHeaders.set('Content-Type', 'application/json');
            fetch(ajaxAction.url, { method: "post", headers: requestHeaders, body: JSON.stringify(ajaxAction.body) }).then(response => {
                return response.text();
            }).then(content => {
                this.modal.getBody().html(content);
                this.afterBody(this, this.modal);
            });
        } else {
            fetch(ajaxAction.url).then(response => {
                return response.text();
            }).then(content => {
                this.modal.getBody().html(content);
                this.afterBody(this, this.modal);
            });
        }

    }

    firstButton() {
        if (this.firstButtonCreation) {
            this.setButtonProperties(this.modal.getFirstButton(), this.firstButtonCreation());
        }
    }

    secondButton() {
        if (this.secondButtonCreation) {
            this.setButtonProperties(this.modal.getSecondButton(), this.secondButtonCreation());
        }
    }

    private setButtonProperties(button: JQuery, buttonProperties: ButtonProperty) {
        if (buttonProperties.disabled) {
            button.css("display", "none");
        } else {
            button.css("display", "block");
            button.text(buttonProperties.text);
            button.off();
            button.on('click',
                (e: Event) => {
                    e.preventDefault();
                    buttonProperties.onClick(this);
                });
        }
    }
}