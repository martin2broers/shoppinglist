﻿import { Modal } from "../Modal";
import { WizardStep } from "./WizardStep";

export class Wizard extends Modal {
    

    constructor(target: string) {
        super(target);
    }

    showWizard(wizardSteps: WizardStep[]) {
        this.show();
        this.showWizardStep(wizardSteps, wizardSteps[0], 0);
    }

    finished(): any {
        this.hide();
    }

    protected showWizardStep(wizardSteps: WizardStep[], wizardStep: WizardStep, index: number) {
        const next = index + 1;
        wizardStep.firstButton();
        wizardStep.secondButton();
        if (index === 0) {
            wizardStep.fillBody(null);
        } else {
            wizardStep.fillBody(wizardSteps[index - 1]);
        }

        wizardStep.registerCompletion((result: any) => {
            if (wizardSteps.length > next) {
                this.showWizardStep(wizardSteps, wizardSteps[next], next);
            }
        });
    }
}