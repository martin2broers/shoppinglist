﻿import { Ingredients } from "../ShoppingListItems/Ingredients";
import { domLoaded } from "../DomLoaded";

class DishesIndex {
    private ingredients: Ingredients; 

    constructor() {
        this.ingredients = new Ingredients();
    }
}

new DishesIndex();