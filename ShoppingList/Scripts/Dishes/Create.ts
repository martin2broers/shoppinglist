﻿import { Ingredients } from "../ShoppingListItems/Ingredients";
import { domLoaded } from "../DomLoaded";
import { IUniqueList, UniqueList } from "../UniqueList";
import { Modal } from "../Modal";
import { Wizard } from "../Wizard/Wizard"
import { WizardStep, AjaxRequest } from "../Wizard/WizardStep"
import { ButtonProperty } from "../ButtonProperty"
import Constants = require("../Constants");


class PostIngredientCreationForDish {
    id = -1;
    amount = -1;
    amountType = -1;
}

class DishesCreate {
    private readonly ingredients: Ingredients;
    private readonly ingredientList: IUniqueList<number>;

    constructor() {
        this.ingredients = new Ingredients();
        this.ingredientList = new UniqueList<number>();
        domLoaded(() => {
            const button = document.getElementsByClassName('btn').item(0);
            if (button) {
                button.addEventListener('click', (event: Event) => this.openIngredients(event));
            }
        });
    }

    private openIngredients(event: Event): void {
        event.preventDefault();

        const wizard = new Wizard("#modal");

        const wizardSteps: WizardStep[] = [];

        const ingredientCreation = new PostIngredientCreationForDish();

        wizardSteps.push(
            new WizardStep(
                wizard,
                "Select ingredient",
                () => new AjaxRequest(Constants.ingredientCreationStep1, null),
                () => new ButtonProperty("", true, () => {}),
                () => new ButtonProperty("", true, () => {}),
                (step: WizardStep, modal: Modal) => {

                    modal.getBody().find("#selection").on('click',
                        (e: Event) => {
                            e.preventDefault();
                            const target = e.currentTarget;
                            if (target) {
                                //@ts-ignore
                                ingredientCreation.id = parseInt($(target).data("id"));
                                step.complete(ingredientCreation.id);
                            }

                        });
                }
            )
        );
        wizardSteps.push(
            new WizardStep(
                wizard,
                "Select amount",
                () => {
                    return new AjaxRequest(Constants.ingredientCreationStep2 + "/" + ingredientCreation.id, null);
                },
                () => new ButtonProperty("next",
                    false,
                    (step: WizardStep) => {
                        ingredientCreation.amount = parseInt(wizard.getBody().find('input[name="Amount"]').val() as string);
                        ingredientCreation.amountType = parseInt(wizard.getBody().find('select[name="AmountType"]').val() as string);
                        step.complete(false);
                    }),
                () => new ButtonProperty("disabled", true, () => {}),
                () => {}
            )
        );
        wizardSteps.push(
            new WizardStep(
                wizard,
                "Finalize",
                () => {
                    return new AjaxRequest(Constants.ingredientCreationStep3 + "/" + ingredientCreation.id, ingredientCreation);
                },
                () => new ButtonProperty("confirm",
                    false,
                    (step: WizardStep) => {
                        const requestHeaders = new Headers();
                        requestHeaders.set('Content-Type', 'application/json');
                        fetch(Constants.ingredientCreationStep4 + "/" + ingredientCreation.id, { method: "post", headers: requestHeaders, body: JSON.stringify(ingredientCreation)})
                            .then((response: Response) => response.json())
                            .then((json: any) => {
                                $('select[name="Ingredients"]')
                                    .append('<option value="' + json.id + '" selected>' + json.name + ' | ' + json.amount + '</option>');
                                wizard.finished();
                            });
                        step.complete(false);
                    }),
                () => new ButtonProperty("disabled", true, () => {}),
                () => {}
            )
        );

        wizard.showWizard(wizardSteps);
    }

    onIngredientSelectionId(id: number): void {
        this.ingredientList.add(id);
        this.rebuildIngredientSelectList();
    }

    rebuildIngredientSelectList(): void {

    }
}

new DishesCreate();