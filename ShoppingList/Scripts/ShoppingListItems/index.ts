﻿import {Ingredients} from './Ingredients';


class IngredientsIndex {
    private ingredients: Ingredients;

    constructor() {
        this.ingredients = new Ingredients();
    }
}

new IngredientsIndex();