﻿
export class Modal {
    private readonly bodyContent: string | null;
    private readonly bodyContentUrl: string | null;
    protected readonly innerModal: JQuery;

    constructor(target: string, bodyContentUrl: string | null = null, bodyContent: string | null = null) {
        this.bodyContent = bodyContent;
        this.bodyContentUrl = bodyContentUrl;
        this.innerModal = $(target);
    }

    getBody(): JQuery {
        return this.innerModal.find(".modal-body");
    }

    getFirstButton(): JQuery {
        return this.innerModal.find("#button1");
    }

    getSecondButton(): JQuery {
        return this.innerModal.find("#button2");
    }

    show() {
        this.innerModal.modal('show');
    }

    hide() {
        this.innerModal.modal('hide');
        this.getBody().html('');
    }
}