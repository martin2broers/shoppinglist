﻿export function domLoaded(callback: () => void) {
    let triggered = false;

    if (document.readyState === "complete") {
        if (!triggered) {
            triggered = true;
            callback();
        }
    }

    window.onload = () => {
        if (!triggered) {
            triggered = true;
            callback();
        }
    };

    document.addEventListener("DOMContentLoaded", () => {
        if (!triggered) {
            triggered = true;
            callback();
        }
    });
}