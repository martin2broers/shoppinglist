﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShoppingList.Core.Abstractions;
using ShoppingList.Shared.Models;
using ShoppingList.Web.Models.Views;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ShoppingList.Web.Controllers
{
    public class IngredientsController : Controller
    {
        private readonly IShoppingListItemsService _shoppingListItemsService;

        public IngredientsController(IShoppingListItemsService shoppingListItemsService)
        {
            _shoppingListItemsService = shoppingListItemsService;
        }


        public async Task<IActionResult> WizardStep1(CancellationToken token)
        {
            var items = await _shoppingListItemsService.AllOrphaned(0, 500, token);
            if (items.Status == ResultStatus.Ok)
            {
                return PartialView("selectionModal/_Step1", items.Result);
            }

            return PartialView("selectionModal/_Step1", new List<ShoppingListItem>());
        }


        public async Task<IActionResult> WizardStep2(int id, CancellationToken token)
        {
            var items = await _shoppingListItemsService.GetById(id, token);
            if (items.Status == ResultStatus.Ok)
            {
                return PartialView("selectionModal/_Step2", items.Result);
            }

            return PartialView("selectionModal/_Step2", new ShoppingListItem());
        }

        [HttpPost]
        public async Task<IActionResult> WizardStep3(int id, [FromBody]IngredientCreation creation, CancellationToken token)
        {
            var item = await _shoppingListItemsService.GetById(id, token);
            if (item.Status == ResultStatus.Ok)
            {
                creation.ItemName = item.Result.ItemName;
                return PartialView("selectionModal/_Step3", creation);
            }

            return PartialView("selectionModal/_Step3", new IngredientCreation());
        }


        [HttpPost]
        public async Task<IActionResult> WizardStep4(int id, [FromBody]IngredientCreation creation, CancellationToken token)
        {
            var item = await _shoppingListItemsService.GetById(id, token);
            if (item.Status == ResultStatus.Ok)
            {
                var obj = item.Result;
                obj.Id = null;
                obj.Amount = creation.Amount;
                obj.AmountType = creation.AmountType;

                var create = await _shoppingListItemsService.Create(obj, token);
                if (create.Status == ResultStatus.Ok)
                {
                    creation.Id = create.Result.Id;
                    creation.ItemName = create.Result.ItemName;
                    return Json(creation);
                }
                else
                {
                    return BadRequest();
                }
            }
            return BadRequest();
        }
    }
}
