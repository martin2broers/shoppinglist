﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShoppingList.Core.Abstractions;
using ShoppingList.Shared.Models;

namespace ShoppingList.Web.Controllers
{
    public class ShoppingListItemsController : Controller
    {
        private readonly IShoppingListItemsService _shoppingListItemsService;

        public ShoppingListItemsController(IShoppingListItemsService shoppingListItemsService)
        {
            _shoppingListItemsService = shoppingListItemsService;
        }

        public async Task<IActionResult> Index(CancellationToken token)
        {
            var items = await _shoppingListItemsService.AllOrphaned(0, 500, token);
            if (items.Status == ResultStatus.Ok)
            {
                return View(items.Result);
            }

            return View(new List<ShoppingListItem>());
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id, CancellationToken token)
        {
            var item = await _shoppingListItemsService.GetById(id, token);
            if (item.Status == ResultStatus.Ok)
            {
                return View(item.Result);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id, CancellationToken token)
        {
            var item = await _shoppingListItemsService.GetById(id, token);

            if (item.Status == ResultStatus.Ok)
            {
                return View(item.Result);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ShoppingListItem item, CancellationToken token)
        {
            var result = await _shoppingListItemsService.Set(item, token);
            if (result.Status == ResultStatus.Ok)
            {
                return RedirectToAction("Details", new {id = item.Id});
            }

            return View(item);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(ShoppingListItem item, CancellationToken token)
        {
            var result = await _shoppingListItemsService.Create(item, token);
            if (result.Status == ResultStatus.Ok)
            {
                return RedirectToAction("Details", new {id = item.Id});
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id, CancellationToken token)
        {
            await _shoppingListItemsService.Remove(id, token);
            return RedirectToAction("Index");
        }
    }
}