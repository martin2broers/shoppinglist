﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShoppingList.Core.Abstractions;
using ShoppingList.Core.Models;
using ShoppingList.Shared.Models;
using ShoppingList.Web.Models;
using ShoppingList.Web.Models.Views;

namespace ShoppingList.Web.Controllers
{
    public class DishesController : Controller
    {
        private readonly IDishesService _service;
        private readonly IShoppingListItemsService _ingredients;

        public DishesController(IDishesService service, IShoppingListItemsService ingredients)
        {
            _service = service;
            _ingredients = ingredients;
        }

        public async Task<IActionResult> Index(CancellationToken token)
        {
            var dishes = await _service.All(0, 500, token);
            if (dishes.Status == ResultStatus.Ok)
            {
                return View(dishes.Result);
            }
            return View(new List<Dish>());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DishCreation dish, CancellationToken token)
        {
            var ingredients = await _ingredients.GetByIds(dish.Ingredients, token);

            if (ingredients.Status == ResultStatus.Ok && 
                dish.FeaturedImage != null && dish.FeaturedImage.IsImage() && dish.FeaturedImage.Length < 8388608) //allow 8mb
            {
                CoreResult<Dish> result;
                await using (var memoryStream = new MemoryStream())
                {
                    await dish.FeaturedImage.CopyToAsync(memoryStream, token);
                    result = await _service.Create(new Dish
                    {
                        Id = dish.Id,
                        FeaturedImage = memoryStream.ToArray(),
                        HowTo = dish.HowTo,
                        Ingredients = ingredients.Result,
                        Name = dish.Name
                    }, token);
                }

                if (result.Status == ResultStatus.Ok)
                {
                    foreach (var ingredient in ingredients.Result)
                    {
                        ingredient.Dish = result.Result;
                        await _ingredients.Set(ingredient, token);
                    }

                    return RedirectToAction("Index");
                }
            }

            return View(dish); //try again 
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, Dish dish, CancellationToken token)
        {
            return View();
        }

        public IActionResult Details()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id, CancellationToken token)
        {
            await _service.Remove(id, token);
            return RedirectToAction("Index");
        }
    }
}