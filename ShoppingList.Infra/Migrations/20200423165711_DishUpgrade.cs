﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShoppingList.Infra.Migrations
{
    public partial class DishUpgrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "FeaturedImage",
                schema: "dbo",
                table: "Dishes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HowTo",
                schema: "dbo",
                table: "Dishes",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ShoppingListItems_Dishes_ShoppingListId",
                schema: "dbo",
                table: "ShoppingListItems",
                column: "ShoppingListId",
                principalSchema: "dbo",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShoppingListItems_Dishes_ShoppingListId",
                schema: "dbo",
                table: "ShoppingListItems");

            migrationBuilder.DropColumn(
                name: "FeaturedImage",
                schema: "dbo",
                table: "Dishes");

            migrationBuilder.DropColumn(
                name: "HowTo",
                schema: "dbo",
                table: "Dishes");
        }
    }
}
