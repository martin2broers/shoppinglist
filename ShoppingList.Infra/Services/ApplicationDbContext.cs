﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Infra.Models.Repo;

namespace ShoppingList.Infra.Services
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Models.Repo.ShoppingList> ShoppingLists { get; set; }
        //public DbSet<IdentityUser> Users { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<ShoppingListItem> ShoppingListItems { get; set; }
       // public DbSet<IdentityUser> IdentityUser { get; set; }
    }
}