﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShoppingList.Shared.Models;

namespace ShoppingList.Infra.Models.Repo
{
    public class RepoResults<T>
    {
        public ResultStatus Status { get; set; }

        public IEnumerable<T> Results { get; set; }
    }
}
