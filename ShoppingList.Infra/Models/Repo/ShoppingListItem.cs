﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ShoppingList.Infra.Abstractions;
using ShoppingList.Shared.Models;

namespace ShoppingList.Infra.Models.Repo
{
    [Table("ShoppingListItems", Schema = "dbo")]
    public class ShoppingListItem: IHasId
    {
        [Key] public int Id { get; set; }

        [ForeignKey("ShoppingListId")]
        public Repo.ShoppingList ShoppingList { get; set; }

        public int? DishId { get; set; }

        [ForeignKey("DishId")]
        public Dish Dish { get; set; }

        [MaxLength(255)]
        public string ItemName { get; set; }

        public double Amount { get; set; }

        public AmountType AmountType { get; set; }
    }
}
