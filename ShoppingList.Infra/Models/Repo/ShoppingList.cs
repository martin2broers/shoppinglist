﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ShoppingList.Infra.Abstractions;

namespace ShoppingList.Infra.Models.Repo
{
    [Table("ShoppingList", Schema = "dbo")]
    public class ShoppingList: IHasId
    {
        [Key] public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }
    }
}
