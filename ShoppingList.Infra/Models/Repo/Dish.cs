﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ShoppingList.Infra.Abstractions;

namespace ShoppingList.Infra.Models.Repo
{
    [Table("Dishes", Schema = "dbo")]
    public class Dish: IHasId
    {
        [Key] public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public byte[] FeaturedImage { get; set; }

        public string HowTo { get; set; }

        [ForeignKey("ShoppingListId")]
        public IEnumerable<Repo.ShoppingListItem> Ingredients { get; set; }
    }
}
