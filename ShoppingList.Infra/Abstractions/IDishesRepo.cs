﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingList.Infra.Models.Repo;

namespace ShoppingList.Infra.Abstractions
{
    public interface IDishesRepo: IBasicSql<Dish>
    {
        
    }
}
