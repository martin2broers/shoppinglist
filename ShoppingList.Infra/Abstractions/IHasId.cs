﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Infra.Abstractions
{
    public interface IHasId
    {
        int Id { get; set; }
    }
}
