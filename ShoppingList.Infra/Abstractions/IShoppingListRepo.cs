﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingList.Infra.Models.Repo;

namespace ShoppingList.Infra.Abstractions
{
    public interface IShoppingListRepo: IBasicSql<Models.Repo.ShoppingList>
    {
        
    }
}
