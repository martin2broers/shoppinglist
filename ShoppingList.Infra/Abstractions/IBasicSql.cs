﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ShoppingList.Infra.Models.Repo;

namespace ShoppingList.Infra.Abstractions
{
    public interface IBasicSql<T>
    {
        Task<RepoResult<T>> GetById(int id, CancellationToken cancellationToken);

        Task<RepoResults<T>> GetByIds(IEnumerable<int> ids, CancellationToken cancellationToken);

        Task<RepoResults<T>> All(int skip, int limit, Expression<Func<T, bool>> filter = null, CancellationToken cancellationToken = default);

        Task<RepoResult<T>> Insert(T entity, CancellationToken cancellationToken);

        Task<RepoResult<T>> Update(T entity, CancellationToken cancellationToken);

        Task<RepoResult<T>> Delete(int id, CancellationToken cancellationToken);
    }
}
