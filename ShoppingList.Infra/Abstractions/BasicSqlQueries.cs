﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Infra.Helpers;
using ShoppingList.Infra.Models.Repo;
using ShoppingList.Shared.Models;
using ShoppingListItem = ShoppingList.Shared.Models.ShoppingListItem;

namespace ShoppingList.Infra.Abstractions
{
    public class BasicSqlQueries<TEntity>: IBasicSql<TEntity> where TEntity : class, IHasId
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public BasicSqlQueries(DbContext context, DbSet<TEntity> dbSet)
        {
            _context = context;
            _dbSet = dbSet;
        }


        public async Task<RepoResult<TEntity>> GetById(int id, CancellationToken cancellationToken)
        {
            var result = await _dbSet.AsNoTracking().Where(x => x.Id == id).SingleOrDefaultAsync(cancellationToken);
            
            return new RepoResult<TEntity>
            {
                Result = result,
                Status = result != null ? ResultStatus.Ok: ResultStatus.Error
            };
        }

        public async Task<RepoResults<TEntity>> GetByIds(IEnumerable<int> ids, CancellationToken cancellationToken)
        {
            var results = ids != null ? await _dbSet.AsNoTracking().Where(x => ids.Contains(x.Id)).ToListAsync(cancellationToken) : new List<TEntity>();

            return new RepoResults<TEntity>
            {
                  Results = results,
                  Status = ResultStatus.Ok
            };
        }

        public async Task<RepoResults<TEntity>> All(int skip, int limit, Expression<Func<TEntity, bool>> filter = null, CancellationToken cancellationToken = default)
        {
            List<TEntity> results;
            if (filter != null)
            {
                results = await _dbSet.AsNoTracking().Where(filter).Skip(skip).Take(limit).ToListAsync(cancellationToken);
            }
            else
            {
                results = await _dbSet.AsNoTracking().Skip(skip).Take(limit).ToListAsync(cancellationToken);
            }
             

            return new RepoResults<TEntity>
            {
                Results = results,
                Status = ResultStatus.Ok
            };
        }

        public async Task<RepoResult<TEntity>> Insert(TEntity entity, CancellationToken cancellationToken)
        {
            _context.Add(entity);

            var created = await _context.SaveChangesAsync(cancellationToken);
            _context.Detach(entity);
            return new RepoResult<TEntity>
            {
                Result = entity,
                Status = created == 1 ? ResultStatus.Ok : ResultStatus.Error
            };
        }

        public async Task<RepoResult<TEntity>> Update(TEntity entity, CancellationToken cancellationToken)
        {
            _context.Detach(entity);
            _context.Update(entity);

            var created = await _context.SaveChangesAsync(cancellationToken);
            return new RepoResult<TEntity>
            {
                Result = entity,
                Status = created == 1 ? ResultStatus.Ok : ResultStatus.Error
            };
        }

        public async Task<RepoResult<TEntity>> Delete(int id, CancellationToken cancellationToken)
        {
            var item = await _dbSet.SingleOrDefaultAsync(x => x.Id == id, cancellationToken);
            var entry = _context.Entry(item);
            entry.State = EntityState.Detached;

            _dbSet.Remove(item);
            var result = await _context.SaveChangesAsync(cancellationToken);

            return new RepoResult<TEntity>
            {
                Result = default,
                Status = result == 1 ? ResultStatus.Ok : ResultStatus.Error
            };
        }
    }
}
