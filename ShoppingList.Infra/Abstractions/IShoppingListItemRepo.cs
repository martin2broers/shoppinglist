﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingList.Infra.Models.Repo;

namespace ShoppingList.Infra.Abstractions
{
    public interface IShoppingListItemRepo: IBasicSql<Models.Repo.ShoppingListItem>
    {
        
    }
}
