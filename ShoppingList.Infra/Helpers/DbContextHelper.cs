﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ShoppingList.Infra.Abstractions;

namespace ShoppingList.Infra.Helpers
{
    public static class DbContextHelper
    {
        public static void Detach<T>(this DbContext context, T entity) where T : class, IHasId
        {
            var detachable = context.Set<T>().Local
                .FirstOrDefault(x => x.Id.Equals(entity.Id));

            if (detachable != null)
            {
                context.Entry(detachable).State = EntityState.Detached;
            }
        }
    }
}
