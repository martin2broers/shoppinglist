﻿using Microsoft.EntityFrameworkCore;
using ShoppingList.Infra.Abstractions;
using ShoppingList.Infra.Models.Repo;
using ShoppingList.Infra.Services;

namespace ShoppingList.Infra.Repositories
{
    public class DishesRepo: BasicSqlQueries<Dish>, IDishesRepo
    {
        public DishesRepo(ApplicationDbContext context) : base(context, context.Dishes)
        {
        }
    }
}
