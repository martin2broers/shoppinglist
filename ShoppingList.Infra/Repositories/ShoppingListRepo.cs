﻿using ShoppingList.Infra.Abstractions;
using ShoppingList.Infra.Services;

namespace ShoppingList.Infra.Repositories
{
    public class ShoppingListRepo : BasicSqlQueries<Models.Repo.ShoppingList>, IShoppingListRepo
    {
        public ShoppingListRepo(ApplicationDbContext context) : base(context, context.ShoppingLists)
        {
        }
    }
}