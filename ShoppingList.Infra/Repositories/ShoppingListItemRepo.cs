﻿using Microsoft.EntityFrameworkCore;
using ShoppingList.Infra.Abstractions;
using ShoppingList.Infra.Models.Repo;
using ShoppingList.Infra.Services;

namespace ShoppingList.Infra.Repositories
{
    public class ShoppingListItemRepo: BasicSqlQueries<ShoppingListItem>, IShoppingListItemRepo
    {
        public ShoppingListItemRepo(ApplicationDbContext context) : base(context, context.ShoppingListItems)
        {
        }
    }
}
