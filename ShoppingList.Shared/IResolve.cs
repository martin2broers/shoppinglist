﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Shared
{
    public class ResolveAble<T>
    {
        public T Value { get; set; }

        public bool IsResolved => Value != null;
    }
}
