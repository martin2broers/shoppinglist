﻿using System.ComponentModel;

namespace ShoppingList.Shared.Models
{
    public class ShoppingListItem
    {
        public int? Id { get; set; }

        public ShoppingList ShoppingList { get; set; }

        public Dish Dish { get; set; }

        [DisplayName("Name")]
        public string ItemName { get; set; }

        public double Amount { get; set; }

        public AmountType AmountType { get; set; }
    }
}
