﻿namespace ShoppingList.Shared.Models
{
    public class ShoppingList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
