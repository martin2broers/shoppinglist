﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Shared.Models
{
    public enum AmountType
    {
        Grams = 0,
        Pieces = 1,
        Milliliters = 2,
        Decileters = 3,
        Liters = 4
    }
}
