﻿using System.Collections.Generic;

namespace ShoppingList.Shared.Models
{
    public class Dish
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public byte[] FeaturedImage { get; set; }

        public string HowTo { get; set; }

        public IEnumerable<ShoppingListItem> Ingredients { get; set; }
    }
}