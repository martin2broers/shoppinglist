﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShoppingList.Shared.Models
{
    public enum ResultStatus
    {
        Ok, Invalid, BadRequest, Error
    }
}
